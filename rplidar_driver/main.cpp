#include "main.h"

int main()
{
	pavo_driver* driver = new  pavo_driver();

	std::string ip;
	printf("输入IP(10.10.10.100):");
	std::cin >> ip;

	int port;
	printf("输入端口(2368):");
	std::cin >> port;

	bool isConnect = false;
	bool isOk = false;

	try
	{
		isConnect = driver->pavo_open(ip, port);
		isOk = true;
	}
	catch (pavo_exception& e)
	{
		printf(e.what());
		isOk = false;
	}

	if (isConnect && isOk) {
		std::cout << "雷达连接成功" << std::endl;

		device_config(driver);
		enable_data(driver, true);
		enable_motor(driver, true);
		set_motor_speed(driver);
		set_ladar_mode(driver, pavo_mode_t::Normal);

		while (isConnect) {
			int time_out = 100;
			std::vector<pavo_response_scan_t> buffer;
			if (driver->get_scanned_data(buffer, time_out)) {
				size_t count = buffer.size();
				for (int pos = 0; pos < (int)count; ++pos) {
					std::cout << "角度:" << buffer[pos].angle << "距离:" << buffer[pos].distance << "强度:" << buffer[pos].intensity << std::endl;
				}
			}
			else {
				fprintf(stderr, "Failed to get Lidar Data\n");
				fflush(stderr);
			}
		}
	}
	else {
		if (isOk) {
			std::string ok_str = get_error_code(driver);
			std::cout << ok_str << std::endl;
		}
		else {
			std::cout << "传参错误，连接失败！" << std::endl;
		}
	}

	if (isOk) {
		enable_data(driver, false);
		enable_motor(driver, false);
		driver->pavo_close();
		delete driver;
	}
	std::cout << "ok!" << std::endl;

}